# Docker Compose Project Boilerplate
### PHP 7.4, MySQL(latest), NGINX(latest), PHPMYADMIN

Get going with your project quickly. Clone this and chuck it in your own git repo.

## DOCKER
Just add your required info identified between <> within docker-compose.override.yml and docker-compose.production.yml.

## MYSQL
Next, add an env_vars file within the mysql folder. This file is .gitignored so you don't go committing your creds to version control like a total dumb-dumb. It needs to look like this:

```
MYSQL_ROOT_PASSWORD=
MYSQL_DATABASE=
MYSQL_USER=
MYSQL_PASSWORD=
```

I'm confident you can fill in the rest.


## PHP.INI

Depending on your location you may need to change `date.timezone` within your php/conf/php.ini-development and php/conf/php.ini-production files.

## BUILD AND RUN

1. From the root of your docker project run `docker-compose build`.
2. Once the build finishes and you didn't encounter any errors 🤞 run `docker-compose up -d`

If all of that worked you'll now be able to add whatever framework or straight up HTML into your src folder and see it at 0.0.0.0 or http://localhost might work for you if your docker install added the correct setting in your /etc/hosts file. Otherwise, head on over to your /etc/hosts file and start mucking about in there. Maybe add something like 0.0.0.0 myderpysite.com? Just a suggestion.

If you're installing a framework be sure to go into `nginx/conf/default.{dev,prod}.template` change `root /var/www/html/<FRONT_CONTROLLER_PATH>;` where FRONT_CONTROLLER_PATH is likely "public" if you're installing Symfony or Laravel. 

## PRODUCTION MODE
Got the nerve to use this in production, do ya? GREAT! Here's what you need to know:

First, you need to create a folder in your src dir as follows: `.well-known/acme-challenge`.

Currently this is setup to use Let's Encrypt. You'll see the conf snippets in `nginx/conf/snippets` and referenced in `nginx/conf/default.prod.template`. You'll want to disable that initially, install certbot on the host and then run

`certbot certonly --webroot -w /path/to/src/dir/in/docker/project/root -d domain.com -d www.domain.com -m your@email.com`

You should see the certs at `/etc/letsencrypt/live/<YOUR_DOMAIN>`

If you got your cert successfully you can now re-enable it in your `default.prod.template`.

You'll also notice that there is a reload script at `nginx/reload`. In there you'll see another another thing you need to edit: DOCKER_COMPOSE_PROJECT_ROOT_PATH_HERE

Once that's done put this script into a cron job set to run once a week. That's sufficient.

Up docker in prod mode `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`

This is just one of many ways to do this. Pull request to make it better. I'll love ya for it. I am aware that there are specific certbot containers out there. Need to explore that road. 

## NOTES

One thing to note when using a framework like Symfony and expecting to be able to run things like `bin/console doctrine:database:whatever`, this will not work outside of the docker context (host context) because the DSN with mysql alias will not resolve. To do this you'll have to go through docker like so:

`docker-compose exec php bin/console doctrine:database:create` as an example. Notice that you can reference the php container by just the 'php' alias here.

If you're not that experienced with docker-compose you need to know that each container has its own alias so that you don't have to figure out the container's IP address and reference that in things like your database DSN. Below is an example of what you'll need in a Symfony .env:

`DATABASE_URL=mysql://username:p455w0rd@mysql:3306/my_database_name`

If you're a MySQL hater and only PostgreSQL will do, it's on you to sort that out in here, bub. But more or less edit docker-compose.yml swapping mysql for postgres referencing the correct image. You can still use the env_vars file, but change the path name (obvi) and var names as POSTGRES_PASSWORD and whatever else is needed.

## NOTES ON PERFORMANCE

Something I encountered while developing a Symfony5 site was that page load time was ~5 seconds. We can all agree that's pretty terrible. So what's wrong? You need to get opcache setup. 

See https://symfony.com/doc/current/performance.html#performance-service-container-single-file

The php Dockerfile installs opcache and the php.ini-{development,production} files are configured accordingly.

Consider user https://github.com/gordalina/cachetool to monitor your opcache and fine tune your opcache settings in php.ini.

See https://tideways.com/profiler/blog/fine-tune-your-opcache-configuration-to-avoid-caching-suprises

Another thing to note when using opcache: whenever you do a rebuild of your project removing the vendor dir and resintalling deps you need to warmup the cache and, when doing so, again, you need to be within the docker context. 

`/usr/local/bin/docker-compose exec php bin/console cache:warmup -e dev`

If you don't do this you'll get a 500 saying "Anonymous class cannot be preloaded".

It's ideal to automate your build tasks with a Makefile or whatever else suits your fancy. Just make that the last step.

## TERMS OF USE
I assume no responsibility for anything. Have fun. Enjoy. 
